$(document).ready(() => {
  const bucketName = 'vinillobucket';
  const s3Region = 'eu-central-1';
  const attacher = $('.attach');


  function refreshVideos() {
    $.ajax({
      url: '/get',
      success(data) {
        console.log(data);
        $(attacher).empty();
        $.each(JSON.parse(data), (index, value) => {
          const link = `https://s3.${s3Region}.amazonaws.com/${bucketName}/${value}`;
          $(attacher).append(`${`${`${'<div class="col-md-12">'
                      + '<h2>'}${value}</h2>`
                      + '<video controls="" preload="auto" height="550" width="100%">'
                      + '<source src="'}${link}" type="video/mp4" >`
                      + '</video>'
                      + '<p></p>'
                      + '<a href="/video/'}${encodeURIComponent(value)}" class="btn btn-raised" role="button"><i class="fas fa-video"></i> Preview & Share</a>`
                      + '</div>');
        });
      }
    });
  }
  function getVideos() {
    $.ajax({
      url: '/refresh',
      success(countDb) {
        $.ajax({
          url: '/get',
          success(data) {
            const apiCount = JSON.parse(data).length;
            console.log('countDb');
            console.log(countDb);
            console.log('apiCount');
            console.log(apiCount);

            if (apiCount > countDb || apiCount < countDb) {
              refreshVideos();
              console.log('truncate & refreshing videos');
            }
          }
        });
      }
    });
  }


  setInterval(() => {
    getVideos();
  }, 5000);
  refreshVideos();
});
